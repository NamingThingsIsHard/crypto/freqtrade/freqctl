#!/bin/bash
# Exit at the first sign of trouble
set -euo pipefail

# Render twine non interactive if we're in the CI
options=""
if [[ -n "$CI_PROJECT_DIR" ]] ; then
    options="--non-interactive"
fi

rm -rf dist build || true
poetry build

if [[ -n "${PUBLISH_USER}" ]] && [[ -n "${PUBLISH_PASSWORD}" ]] ; then
  poetry publish -u "${PUBLISH_USER}" -p "${PUBLISH_PASSWORD}"
else
  poetry publish
fi
