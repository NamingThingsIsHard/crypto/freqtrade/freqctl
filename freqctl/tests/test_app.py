"""
freqctl
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import unittest

from cliff.commandmanager import CommandManager

from freqctl.app import FreqCtl


class FreqCtlTestcases(unittest.TestCase):
    """
    Make sure the FreqCtl application behaves properly
    """

    def test_name(self):
        """Ensure the app has the right name"""
        self.assertEqual(FreqCtl.NAME, "freqctl")

    def test_command_manager(self):
        """Ensure the command manager is actually a CommandManager"""
        ctl = FreqCtl()
        self.assertIsInstance(ctl.command_manager, CommandManager)
