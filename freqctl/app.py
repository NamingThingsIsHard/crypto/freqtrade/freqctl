"""
freqctl
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import logging

import pkg_resources
from cliff.app import App
from cliff.commandmanager import CommandManager

COMMAND_SUFFIX = "Command"
VERSION = pkg_resources.get_distribution("freqctl").version


class FreqCtlCommandManager(CommandManager):
    """
    Customized manager to load freqctl commands
    """

    def load_commands(self, namespace):
        """
        Creates commands that will be available to freqctl.

        Commands should be created in freqctl.commands and then added here with
        self.add_command("command", command_object)

        :type namespace: basestring
        """


# Set name here to get the right name in the log messages
App.NAME = "freqctl"


# pylint: disable=missing-class-docstring
class FreqCtl(App):
    CONSOLE_MESSAGE_FORMAT = logging.BASIC_FORMAT

    def __init__(self):
        super().__init__(
            description="A master for the freqtrade slaves",
            version=VERSION,
            command_manager=FreqCtlCommandManager("freqctl.commands"),
        )
